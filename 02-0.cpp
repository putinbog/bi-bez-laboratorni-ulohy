#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <cstdlib>
#include <ctime>
#include <openssl/sha.h>

using namespace std;

string random_key( const size_t len )
{
  static const char alphanum[] =
    "0123456789"
    "abcdef";

  static const int stringLength = sizeof(alphanum) - 1;
  string result = "";

  for ( size_t i = 0; i < len; i++ )
    result += alphanum[rand() % stringLength];
  return result;
}

bool sha256 ( string & str, stringstream & ss, string & key )
{
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, str.c_str(), str.size());
    SHA256_Final(hash, &sha256);

    ss . str ("");

    for(size_t i = 0; i < key . length() / 2; i++)
      ss << hex << setw(key . length() / 2) << setfill ( '0' ) << (int)hash[i];

    if ( ss.str() == key )
    {
      for ( int i = key . length() / 2; i < SHA256_DIGEST_LENGTH; i++ )
        ss << hex << setw(2) << setfill('0') << (int)hash[i];

      cout << "FOUND:" << endl;
      cout << "[" << ss . str() << "]" << " @ " << str << endl;
      return true;
    }

    cout << "[" << ss.str() << "]" << endl;
    return false;
}

int main ( int argc, char * argv[] )
{
    if ( argc != 2 )
    {
      cout << "Usage: " << argv[0] << " [key]" << endl;
      return 0;
    }

    stringstream  ss;
    string        num;
    string        key ( argv[1] );


    srand( time(NULL) );

    do
    {
      num = random_key ( key.length() );
    }
    while ( ! sha256 ( num, ss, key ) );

    return 0;

}
