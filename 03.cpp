#include <iostream>
#include <fstream>
#include <openssl/evp.h>
#include <cstring>

using namespace std;

struct header
{
  uint16_t magicNumber;
  uint32_t size;
  uint32_t reserved;
  uint32_t begin;
  uint8_t  * format;
};

bool readHeader ( header & fileHeader, ifstream & in )
{
  if ( ! in . read ( (char *) &fileHeader . magicNumber, sizeof ( uint16_t ) )
    || ! in . read ( (char *) &fileHeader . size, sizeof ( uint32_t ) )
    || ! in . read ( (char *) &fileHeader . reserved, sizeof ( uint32_t ) )
    || ! in . read ( (char *) &fileHeader . begin, sizeof ( uint32_t ) )
    || fileHeader . begin <= 0
    || fileHeader . begin > fileHeader . size )
    return false;
  return true;
}

bool writeHeader ( header & fileHeader, ofstream & out )
{
  if ( ! out . write ( (char *) &fileHeader . magicNumber, sizeof ( uint16_t ) )
    || ! out . write ( (char *) &fileHeader . size, sizeof ( uint32_t ) )
    || ! out . write ( (char *) &fileHeader . reserved, sizeof ( uint32_t ) )
    || ! out . write ( (char *) &fileHeader . begin, sizeof ( uint32_t ) ) )
    return false;
  return true;
}

bool readFormat ( header & fileHeader, ifstream & in, ofstream & out )
{
  fileHeader . format = new uint8_t[fileHeader . begin - 14];
  if ( ! in . read ( (char *) fileHeader . format, (fileHeader . begin - 14) * sizeof ( uint8_t ) )
    || ! out . write ( (char *) fileHeader . format, (fileHeader . begin - 14) * sizeof ( uint8_t ) ) )
    return false;
  return true;
}

void encryptPixels ( ifstream & in, ofstream & out, char * cph )
{
  unsigned char ot[1024];
  unsigned char st[1024];
  unsigned char key[EVP_MAX_KEY_LENGTH] = "Tajny klic";
  unsigned char iv[EVP_MAX_IV_LENGTH] = "123456789";
  int stLength = 0;

  const EVP_CIPHER * cipher;
  if ( strcmp ( cph, "-ecb" ) == 0 )
    cipher = EVP_des_ecb();
  else
    cipher = EVP_des_cbc();
  EVP_CIPHER_CTX ctx;

  EVP_EncryptInit_ex ( &ctx, cipher, nullptr, key, iv );

  while ( ! in . eof () )
  {
    if ( ! in . read ( (char *) &ot, 1024 ) )
      break;

    EVP_EncryptUpdate ( &ctx, st, &stLength, ot, 1024 );

    if ( ! out . write ( (char *) &st, 1024 ) )
    {
      cout << "Error while writing." << endl;
      return;
    }
  }

  EVP_EncryptFinal_ex ( &ctx, st, &stLength );
}

void decryptPixels ( ifstream & in, ofstream & out, char * cph )
{
  unsigned char ot[1024];
  unsigned char st[1024];
  unsigned char key[EVP_MAX_KEY_LENGTH] = "Tajny klic";
  unsigned char iv[EVP_MAX_IV_LENGTH] = "123456789";
  int stLength = 0;

  const EVP_CIPHER * cipher;
  if ( strcmp ( cph, "-ecb" ) == 0 )
    cipher = EVP_des_ecb();
  else
    cipher = EVP_des_cbc();
  EVP_CIPHER_CTX ctx;
  
  EVP_DecryptInit_ex(&ctx, cipher, nullptr, key, iv);
  while ( ! in . eof () )
  {
    in . read ( (char *) &st, 1024 );
    EVP_DecryptUpdate ( &ctx, ot, &stLength, st, 1024 );

    if ( ! out . write ( (char *) &ot, 1024 ) )
    {
      cout << "Error while writing." << endl;
      return;
    }
  }
  EVP_DecryptFinal_ex(&ctx, ot, &stLength);
}

int main ( int argc, char * argv[] )
{
  if ( argc != 5
    || ( strcmp ( argv[1], "-e" ) != 0
      && strcmp ( argv[1], "-d" ) != 0 )
    || ( strcmp ( argv[2], "-ecb" ) != 0
      && strcmp ( argv[2], "-cbc" ) != 0 ) )
  {
    cout << "Usage: " << argv[0] << " [-e, -d] [-ecb, -cbc] [input_file.bmp] [output_file.bmp]" << endl;
    return 0;
  }

  header fileHeader;
  ifstream in ( argv[3], ios::binary );
  ofstream out ( argv[4], ios::binary );

  if ( in . fail ()
    || out . fail ()
    || ! readHeader ( fileHeader, in )
    || ! writeHeader ( fileHeader, out )
    || ! readFormat ( fileHeader, in, out ) )
  {
    in . close();
    out . close();
    cerr << "Error reading a file.\n";
    return 1;
  }

  unsigned char st[1024];

  if ( strcmp ( argv[1], "-e" ) == 0 )
    encryptPixels( in, out, argv[2] );
  else
    decryptPixels ( in, out, argv[2] );

  in . close();
  out . close();
  return 0;
}

