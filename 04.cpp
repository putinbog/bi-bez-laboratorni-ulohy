#include <iostream>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <cstring>

#define BUFFER_SIZE 512

using namespace std;

FILE * openFile(const string &filename, const string &type) {
    FILE *file;
    if (!(file = fopen(filename.c_str(), type.c_str()))) {
        printf("Error opening %s\n", filename.c_str());
        exit (1);
    }
    return file;
}

/// *************** ENCRYPT PROGRAM *********************
void encrypt(const string & publicKeyPath, const string & inputFilePath, const string & outputFilePath )
{
  printf("************** ENCTYPTION **************\n" );
    /// READ KEY
    FILE *  f = openFile(publicKeyPath, "r");
    EVP_PKEY *pubKey = PEM_read_PUBKEY(f, nullptr, nullptr, nullptr);
    if ( ! pubKey )
      cout << "Error public key" << endl;
    fclose(f);

    /// CIPHER INITIALIZATION
    OpenSSL_add_all_ciphers();
    const EVP_CIPHER *cipher = EVP_aes_256_cbc();
    if ( ! cipher )
      cout << "No cipher" << endl;


    EVP_CIPHER_CTX ctx;
    EVP_CIPHER_CTX_init(&ctx);
    unsigned char * encryptKey = new unsigned char [EVP_PKEY_size(pubKey)];
    unsigned char iv[EVP_MAX_IV_LENGTH];

    int encryptKeyLen;
    if (!(EVP_SealInit(&ctx, cipher, &encryptKey, &encryptKeyLen, iv, &pubKey, 1)))
        exit(3);


    /// INPUT OUTPUT FILES
    FILE *input = openFile(inputFilePath, "r");
    FILE *output = openFile(outputFilePath, "w");


    /// WRITE HEAD
    string cipherName = "aes-256-cbc";
    fwrite(cipherName.c_str(), sizeof(unsigned char), strlen(cipherName.c_str()), output);
    fprintf(output, "%d", encryptKeyLen);
    fwrite(encryptKey, sizeof(unsigned char), static_cast<size_t>(encryptKeyLen), output);
    fwrite(iv, sizeof(unsigned char), EVP_MAX_IV_LENGTH, output);

    // printf ("KEY: ");
    // for (int i = 0; i < encryptKeyLen; i++)
    //     printf("%02x", encryptKey[i]);
    // printf("\n\nIV:");
    // for (unsigned char i : iv)
    //     printf("%02x", i);

    /// CIPHER
    int res;

    /* cipher main part */
    unsigned char buffer[BUFFER_SIZE];
    unsigned char bufferOut[BUFFER_SIZE + EVP_MAX_BLOCK_LENGTH];
    int length = 0;
    int encrytDataLen = 0;
    while ((res = static_cast<int>(fread(buffer, sizeof(unsigned char), BUFFER_SIZE, input)))) {
        if ( 1 != EVP_SealUpdate(&ctx, bufferOut, &encrytDataLen, buffer, res)) exit(4);
        fwrite(bufferOut, sizeof(unsigned char), static_cast<size_t>(encrytDataLen), output);
        length += encrytDataLen;
    }

    if (!EVP_SealFinal(&ctx, bufferOut, &encrytDataLen)) exit(5);
    fwrite(bufferOut, sizeof(unsigned char), static_cast<size_t>(encrytDataLen), output);
    length += encrytDataLen;
    free(encryptKey);


    // printf("\n\nLENGTH: %d\n", length);
    fclose(input);
    fclose(output);
}

/// ************* DECRYPT PROGRAM ****************
void decrypt(const string & privateKeyPath, const string & inputFilePath, const string & outputFilePath )
{
  printf("************** DECRYPTION **************\n" );

  FILE *  f = openFile(privateKeyPath, "r");
  EVP_PKEY *privKey = PEM_read_PrivateKey(f, nullptr, nullptr, nullptr);
  fclose(f);

  /// READ HEAD
  FILE *input = openFile(inputFilePath, "rb");
  unsigned char iv[EVP_MAX_IV_LENGTH];

  unsigned int encryptKeyLen;
  fscanf(input, "%u", &encryptKeyLen);
  auto *encryptKey = (unsigned char *)malloc(encryptKeyLen);
  fread(encryptKey, sizeof(unsigned char), encryptKeyLen, input);

  fread(iv, sizeof(unsigned char), EVP_MAX_IV_LENGTH, input);

  /* debug info */
  // printf("KEY: ");
  // for (int i = 0; i < encryptKeyLen; i++)
  //     printf("%02x", encryptKey[i]);
  // printf("\n\nIV:");
  // for (unsigned char i : iv)
  //     printf("%02x", i);


  /// CYPHER INITIALIZATION
  const EVP_CIPHER *cipher = EVP_aes_256_cbc();
  EVP_CIPHER_CTX ctx;
  EVP_CIPHER_CTX_init(&ctx);
  if (!EVP_OpenInit(&ctx, cipher, encryptKey, encryptKeyLen, iv, privKey)) exit(7);


  /// DECIPHER
  FILE * output = openFile(outputFilePath, "w");

  /* decipher main part */
  unsigned char buffer[BUFFER_SIZE];
  unsigned char bufferOut[BUFFER_SIZE + EVP_MAX_BLOCK_LENGTH];
  int res, length = 0, decryptedDataLength = 0;
  while ((res = static_cast<int>(fread(buffer, sizeof(unsigned char), BUFFER_SIZE, input))) > 0) {
      if (!EVP_OpenUpdate(&ctx, bufferOut, &decryptedDataLength, buffer, res)) exit(8);
      fwrite(bufferOut, sizeof(unsigned char), static_cast<size_t>(decryptedDataLength), output);
      length += decryptedDataLength;
  }

  /* decipher last block */
  if (!EVP_OpenFinal(&ctx, bufferOut, &decryptedDataLength)) exit(9);
  fwrite(bufferOut, sizeof(unsigned char), static_cast<size_t>(decryptedDataLength), output);
  length += decryptedDataLength;


  printf("\n\nLENGTH: %d\n", length);

  fclose(input);
  fclose(output);
  free(encryptKey);
}

int main ( int argc, char *argv[] )
{
  if ( argc != 5 || ( strcmp( argv[1], "e" ) && strcmp ( argv[1], "d") ) )
  {
    printf("usage 1: \"e {publicKeyPath} {inputFileName} {outputFileName}\"\n"
      "usage 2: \"d {privateKeyPath} {inputFileName} {outputFileName}\"\n");
    return 1;
  }

  /*
    ./a.out e pubkey.pem inputFile encryptedFile
    ./a.out d privkey.pem encryptedFile decryptedFile
    diff inputFile decryptedFile
  */

  if( ! strcmp( argv[1], "e" ) )
    encrypt( argv[2], argv[3], argv[4] );
  else if ( ! strcmp ( argv[1], "d" )  )
    decrypt( argv[2], argv[3], argv[4] );

  return 0;
}
