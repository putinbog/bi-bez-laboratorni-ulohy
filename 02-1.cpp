#include <iostream>
#include <iomanip>
#include <sstream>
#include <openssl/evp.h>
#include <cstring>

using namespace std;

int charToHex(const char c)
{
  if (isdigit(c))
      return c-'0';

  switch (c)
  {
      case 'A':
      case 'a':
          return 10;
      case 'B':
      case 'b':
          return 11;
      case 'C':
      case 'c':
          return 12;
      case 'D':
      case 'd':
          return 13;
      case 'E':
      case 'e':
          return 14;
      case 'F':
      case 'f':
          return 15;
  }
  return 0;
}

void print ( unsigned char * st, int len )
{
  for ( int i = 0; i < len; i++)
      printf("%02hhx", st[i]);
  printf("\n\n");
}

int encode ( unsigned char* ot, unsigned char* st )
{
  OpenSSL_add_all_ciphers();
  EVP_CIPHER_CTX *ctx;
  ctx = EVP_CIPHER_CTX_new();
  if (ctx == NULL) exit(2);

  unsigned char key[EVP_MAX_KEY_LENGTH] = "s6j-8=$Uu]^V8QSw";
  unsigned char iv[EVP_MAX_IV_LENGTH] = "123456789";
  const EVP_CIPHER* cipher = EVP_get_cipherbyname("RC4");

  int otlen = strlen((const char*) ot);
  int stlen = 0;
  int tmp   = 0;

  int res = EVP_EncryptInit_ex(ctx, cipher, NULL, key, iv);
  if(res != 1) exit(3);
  res = EVP_EncryptUpdate(ctx, st, &tmp, ot, otlen);
  if(res != 1) exit(4);
  stlen += tmp;
  res = EVP_EncryptFinal_ex(ctx, st + stlen, &tmp);
  if(res != 1) exit(5);
  stlen += tmp;

  return stlen;
}

unsigned char * readLine ()
{
  string line;
  getline ( cin, line );

  unsigned char * result = new unsigned char [1024];
  strcpy ( (char*) result, line.c_str() );
  return result;
}

int main ( void )
{
  cout << "Enter plain text:" << endl;
  unsigned char * ot1;
  ot1 = readLine(); //Lorem ipsum dolor sitamet, co

  unsigned char st1[1024];
  int length1 = encode( ot1, st1);

  printf ( "Opened Message is: \"%s\"\n", ot1 );
  print ( st1, length1 );

  cout << "Enter cipher text1:" << endl;
  string hex_st1;
  getline ( cin, hex_st1 );

  cout << "Enter cipher text2:" << endl;
  string hex_st2;
  //d2ec01fc13fb0761b1ef3a633cf0ad0be988adeb76efd0398cdd8f2a50
  getline ( cin, hex_st2 );

  cout << endl << endl;

  for (uint i = 0, j = 0; i < hex_st1.length(); i += 2, j++)
  {
      int byte1 = charToHex(hex_st1[i]) * 16 + charToHex(hex_st1[i+1]);
      int byte2 = charToHex(hex_st2[i]) * 16 + charToHex(hex_st2[i+1]);
      printf("%c", (byte1 ^ ot1[j]) ^ byte2);
  }

  printf ( "\n" );
  return 0;
}
